#!/usr/bin/env bash
EXP_LIMIT=30;
DATE_NOW=$(date -d "now" +%s)
EXP_DATE=$(date -d "`certtool -i < ${4}/${2}.crt | grep "Not After" | cut -d ' ' -f4-`" +%s)
EXP_DAYS=$(echo \( $EXP_DATE - $DATE_NOW \) / 86400 |bc)

echo "Checking expiration date for $DOMAIN..."

if [ "$EXP_DAYS" -gt "$EXP_LIMIT" ] ; then
	echo "The certificate is up to date, no need for renewal ($EXP_DAYS days left)."
	exit 0;
else
	echo "The certificate for $DOMAIN is about to expire soon. Starting renewal script..."
    python /opt/acme/acme_tiny.py --account-key $1 --csr "/etc/x509/requests/${2}.csr" --acme-dir $3 > "${4}/${2}.crt"
	if [ $? -eq 0 ]; then
		cat "${4}/${2}.crt" "$5" > "${4}/${2}-full.pem"
	else
		exit 1;
	fi
	exit 0;
fi

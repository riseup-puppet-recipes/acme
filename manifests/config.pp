# Cron setup
class acme::config inherits acme {
  cron { 'signing_request':
    name     => "cert signing ${::acme::domain}",
    command  => "bash ${::acme::params::acme_scripts_dir}/${::acme::params::acme_script} \
${::acme::params::acme_conf_directory}/${::acme::params::acme_client_key} \
${domain} \
${::acme::params::x509_request} ${::acme::params::acme_proofs_dir} \
${::acme::params::x509_certificates_dir} \
${::acme::params::acme_conf_directory}/intermediate.pem",
    monthday => '1'
  }
}

# ACME puppet module
class acme (
  $domain                = $acme::params::domain,
  $x509_group            = $acme::params::x509_group,
  $x509_directory        = $acme::params::x509_directory,
  $x509_request          = $acme::params::x509_request,
  $x509_certificates_dir = $acme::params::x509_certificates_dir,
  $acme_conf_directory   = $acme::params::acme_conf_directory,
  $acme_client_key       = $acme::params::acme_client_key,
  $acme_provider         = $acme::params::acme_provider,
  $acme_proofs_dir       = $acme::params::acme_proofs_dir,
  $acme_scripts_dir      = $acme::params::acme_scripts_dir,
  $acme_script           = $acme::params::acme_script,
  $acme_provider_name    = $acme::params::acme_provider_name
) inherits acme::params {
  anchor { 'acme::begin': } ->
  class { '::acme::install': } ->
  class { '::acme::config': } ->
  anchor { 'acme::end': }
}

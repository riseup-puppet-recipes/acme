# ACME Install
class acme::install inherits acme {

  include x509::base

  file {
    $acme_conf_directory:
      ensure  => 'directory',
      owner   => 'root',
      group   => 'root',
      require => Class[x509::base];

    "${acme_conf_directory}/${acme_client_key}":
      ensure => file,
      owner  => 'root',
      group  => 'root';

    $acme_proofs_dir:
      ensure  => 'directory',
      require => Group[$x509_group];

    $acme_scripts_dir:
      ensure => 'directory',
      owner  => 'root',
      group  => 'root';

    "${acme_scripts_dir}/acme_tiny.py":
      ensure => file,
      mode   => '0700',
      owner  => 'root',
      group  => 'root',
      source => 'puppet:///modules/acme/acme_tiny.py';

    "${acme_scripts_dir}/${acme_script}":
      ensure => file,
      mode   => '0700',
      owner  => 'root',
      group  => 'root',
      source => 'puppet:///modules/acme/acme.sh';
  }

  case $acme_provider_name {
    'letsencrypt': {
      file { "${acme_conf_directory}/intermediate.pem":
        ensure  => file,
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        source => 'puppet:///modules/acme/lets-encrypt-x3-cross-signed.pem';
      }
    }
  }
}

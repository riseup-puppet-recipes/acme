# ACME default  parameters
class acme::params {
  $domain                = undef
  $x509_group            = 'ssl-cert'
  $x509_directory        = '/etc/x509'
  $x509_request          = undef
  $x509_certificates_dir = '/etc/x509/certs'
  $acme_conf_directory   = '/etc/x509/acme'
  $acme_client_key       = 'client.key'
  $acme_provider         = 'https://acme-v01.api.letsencrypt.org'
  $acme_proofs_dir       = '/etc/x509/validation'
  $acme_scripts_dir      = '/opt/acme'
  $acme_script           = 'acme.sh'
  $acme_provider_name    = 'letsencrypt'
}
